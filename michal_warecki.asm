org 100h

%define cr 13
%define lf 10
%define nwln cr, lf

section .data
AttendeeName db "Micha� Warecki", nwln, '$'

section .text
global _start

_start:
    mov ah, 9
    mov dx, AttendeeName
    int 21h
    mov ah, 0
    int 16h
    mov ax, 4C00h
    int 21h